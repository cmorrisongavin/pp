Daily Click is an iPhone app designed to enhance user engagement and build a daily habit. Users will receive a notification at the same time every day, reminding them to open the app and click a single button. The app tracks each click, providing users with a visual representation of their streak and progress over time.
Key Features

    Daily Notification: A notification will be sent to the user at a predefined time every day.
    Single Button Click: A central button that the user needs to click once they open the app.
    Streak Tracking: Track the user's streak of consecutive days they have clicked the button.
    Progress Visualization: Display the user's progress in a visually appealing way (e.g., calendar view, streak counter).
    Settings: Allow users to customize the notification time.

<img width="1123" alt="Screenshot 2024-05-27 at 15 05 27" src="https://github.com/oliverfknowl/dailyclick/assets/143619869/b9e14979-bf30-4c6a-95ba-85d8c8f1c5e5">


Implementation Steps
1. Setting Up the Project

    Create a new Xcode project: Open Xcode and create a new iOS project.
    Choose the Single View App template: This will give you a simple starting point.

2. User Interface

    Storyboard/SwiftUI: Use Interface Builder or SwiftUI to design the main screen with a single button in the center.
    Label for Streaks: Add a label to display the current streak.
    Settings Screen: Add a settings screen for customizing notification time (use a UIDatePicker).

3. Notification Setup

    Request Notification Permissions: In your AppDelegate.swift or SceneDelegate.swift, request permission to send notifications.
    Schedule Daily Notification: Use UNUserNotificationCenter to schedule a notification at the same time every day.

4. Handling Button Click

    Action for Button Click: Connect the button to an action in your ViewController.
    Track Clicks: Save the date of the last click using UserDefaults or Core Data to track the user's streak.

5. Streak Logic

    Calculate Streak: Each time the button is clicked, check the date and update the streak accordingly. Reset if a day is missed.
    Update UI: Reflect the streak count on the label.

6. Background Tasks (Optional)

    Ensure Notifications in Background: Implement background tasks to ensure notifications are scheduled and handled properly even if the app is not active.

7. User Settings

    Settings Screen: Allow users to set their preferred notification time.
    Save Settings: Save the chosen time in UserDefaults and reschedule notifications if the time is changed.
