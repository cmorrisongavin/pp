from flask import Flask

def create_app(config_name=None):
    app = Flask(__name__)

    # Load configuration
    if config_name is None:
        config_name = os.getenv('FLASK_CONFIG', 'default')
    
    from .config import config
    app.config.from_object(config[config_name])

    # Import and register blueprints
    from .routes.home import home_bp
    from .routes.timestamps import timestamps_bp

    app.register_blueprint(home_bp)
    app.register_blueprint(timestamps_bp, url_prefix='/timestamps')

    return app
