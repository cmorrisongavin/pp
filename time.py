from flask import Flask, jsonify, request
from datetime import datetime

app = Flask(__name__)

# Sample data to store timestamps
timestamps = []

# Home route
@app.route('/')
def home():
    return "Welcome to the Timestamp API!"

# Get all timestamps
@app.route('/timestamps', methods=['GET'])
def get_timestamps():
    return jsonify(timestamps)

# Add new timestamp
@app.route('/timestamps', methods=['POST'])
def add_timestamp():
    new_timestamp = {
        "id": len(timestamps) + 1,
        "timestamp": datetime.utcnow().isoformat()
    }
    timestamps.append(new_timestamp)
    return jsonify(new_timestamp), 201

# Get timestamp by id
@app.route('/timestamps/<int:timestamp_id>', methods=['GET'])
def get_timestamp_by_id(timestamp_id):
    item = next((t for t in timestamps if t['id'] == timestamp_id), None)
    return jsonify(item) if item else ('', 404)

# Update timestamp by id
@app.route('/timestamps/<int:timestamp_id>', methods=['PUT'])
def update_timestamp(timestamp_id):
    item = next((t for t in timestamps if t['id'] == timestamp_id), None)
    if item:
        item['timestamp'] = datetime.utcnow().isoformat()
        return jsonify(item)
    else:
        return ('', 404)

# Delete timestamp by id
@app.route('/timestamps/<int:timestamp_id>', methods=['DELETE'])
def delete_timestamp(timestamp_id):
    global timestamps
    timestamps = [t for t in timestamps if t['id'] != timestamp_id]
    return ('', 204)

if __name__ == '__main__':
    app.run(debug=True)
